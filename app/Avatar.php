<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    protected $fillable = ['path'];

    public $timestamps = false;

    public function employee()
    {
    	return $this->belongsTo('App\Employee');
    }
}
