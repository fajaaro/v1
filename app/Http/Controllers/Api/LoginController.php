<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
    	$credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            Cache::add('userLogin', Auth::user());

            return redirect()->route('home');
        } 
        
        return redirect()->route('login');
    }
}
