<?php

namespace App\Http\Controllers\Api;

use App\Avatar;
use App\Employee;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmployee;
use App\Http\Resources\EmployeeResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show', 'search']);
    }

    public function index()
    {
        return EmployeeResource::collection(Employee::with('avatar')->get());
    }

    public function store(StoreEmployee $request)
    {         
        $employee = new Employee;
        $employee->full_name = $request->input('full_name');
        $employee->nick_name = $request->input('nick_name');
        $employee->birthdate = "{$request->input('year')}-{$request->input('month')}-{$request->input('date')}";
        $employee->age = Carbon::parse($employee->birthdate)->age;
        $employee->address = $request->input('address');
        $employee->phone_number = $request->input('phone_number');
        $employee->save();

        if ($request->has('avatar')) {
            $file = $request->file('avatar');

            $avatar = new Avatar;
            $avatar->path = Storage::putFileAs('avatars', $file, "{$employee->id}.{$file->guessExtension()}");
            $employee->avatar()->save($avatar);
        }

        $employee->avatar;

        return new EmployeeResource($employee);
    }

    public function show($id)
    {
        $employee = Employee::with('avatar')->find($id);

        if ($employee == null) {
            return response()->json([
                'message' => 'Employee not found.'
            ]);
        }

        return new EmployeeResource($employee);
    }

    public function update(Request $request, Employee $employee)
    {
        if ($request->has('avatar')) {
            $file = $request->file('avatar');

            Storage::delete($employee->avatar->path);
            
            $employee->avatar->path = Storage::putFileAs('avatars', $file, "{$employee->id}.{$file->guessExtension()}");
            $employee->avatar->save();
        }

        $employee->nick_name = $request->input('nick_name');
        $employee->address = $request->input('address');
        $employee->phone_number = $request->input('phone_number');
        $employee->save();

        return new EmployeeResource($employee);
    }

    public function destroy($id)
    {
        $employee = Employee::find($id);

        if ($employee == null) {
            return response()->json([
                'message' => 'Employee not exist.'
            ]);
        }

        Storage::delete($employee->avatar->path);

        $employeeFullName = $employee->full_name;

        $employee->delete();

        return response()->json([
            'action' => 'Firing Employee!',
            'full_name' => $employeeFullName,
        ]);
    }

    public function search(Request $request)
    {
        $employees = Employee::where('full_name', 'like', $request->input('employee_name') . '%')
                            ->orWhere('full_name', 'like', '%' . $request->input('employee_name'))
                            ->orWhere('full_name', 'like', '%' . $request->input('employee_name') . '%')
                            ->get();

        return EmployeeResource::collection($employees);
    }
}
