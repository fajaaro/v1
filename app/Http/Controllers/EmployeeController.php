<?php

namespace App\Http\Controllers;

use App\Employee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class EmployeeController extends Controller
{
    public function index()
    {
        if (Cache::has('userLogin')) {
            $user = Cache::pull('userLogin');
            Auth::login($user);

            session()->flash('status', 'You are logged in!');

            return redirect()->route('home');
        }

        $employees = Employee::with('avatar')->get();

        return view('home', compact('employees'));
    }

    public function show(Employee $employee)
    {
        return view('employees.show', compact('employee'));
    }
}
