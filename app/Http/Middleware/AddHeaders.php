<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AddHeaders
{
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        // dd($request->user());
        // $response->header('Authorization', 'Bearer ' . $request->user()->makeVisible(['api_token']));
        $response->header('Accept', 'application/json');

        return $response;
    }
}
