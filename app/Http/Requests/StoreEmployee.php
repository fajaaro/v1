<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployee extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'full_name' => 'required',
            'nick_name' => 'required',
            'date' => 'required',
            'month' => 'required',
            'year' => 'required',
            'address' => 'required',
            'phone_number' => 'required',
            'avatar' => 'required',
        ];
    }
}
