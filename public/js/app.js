$(document).ready(function() {
	$('.logout-link').on('click', function() {
		$('.logout-form').submit();
	});
	
 	let added = 1;

	$('.create-employee-form').on('submit', function(e) {
		e.preventDefault();

		$.ajax({
			url : '/api/employees',
			method : 'post',
			data : new FormData(this),
		   	dataType : 'json',
		   	contentType : false,
		   	cache : false,
		   	processData : false,
			success : function(response) {
				let data = response.data;

				let lastNum = parseInt($('.employee-num').last().html());
				if (isNaN(lastNum)) lastNum = 0;

				let numRow = lastNum + added;
				added++;

				let content = `
					<tr>
                        <th scope="row">${numRow}</th>
                        <th><img class="avatar" src="http://localhost:8000/storage/${data.avatar.path}"></th>
                        <td><a href="http://localhost:8000/employees/${data.id}">${data.full_name}</a></td>
                        <td>${data.nick_name}</td>
                        <td>${data.age}</td>
                        <td>${data.birthdate}</td>
                        <td>${data.address}</td>
                        <td>${data.phone_number}</td>
                    </tr>
				`;

				if ($('.employee-table-body').children().length == 0) {
					$('.no-employees').remove();
				}

				$('.employee-table-body').append(content);

				// hide modal and go to the bottom of the page with animation
				$('#modalAddEmployee').modal('hide');
				$('html, body').animate({
        			scrollTop : $(".footer").offset().top
    			}, 1000);

				// get api_token
				let api_token = $('.api_token').attr('value');

    			// clear all input form
    			$('.create-employee-form').trigger('reset');

    			// set api_token
    			$('.api_token').attr('value', api_token);
			},
			error : function(response) {
				console.log('error');
			}
		});
	});

	$('.update-employee-form').on('submit', function(e) {
		e.preventDefault();

		$.ajax({
			url : `/api/employees/${$(this).attr('id')}`,
			method : 'post',
			data : new FormData(this),
		   	dataType : 'json',
		   	contentType : false,
		   	cache : false,
		   	processData : false,
			success : function(response) {
				let data = response.data;

				$('#modalUpdateEmployee').modal('hide');

				location.reload();
			},
			error : function(response) {
				console.log('error');
			}
		});
	});

	$('.delete-employee-form').on('submit', function(e) {
		e.preventDefault();

		$.ajax({
			url : `/api/employees/${$(this).attr('id')}`,
			method : 'post',
			data : new FormData(this),
			dataType : 'json',
		   	contentType : false,
		   	cache : false,
		   	processData : false,
			success : function(response) {
				swal(`${response.action}`, `You fired ${response.full_name}!`, "success")
				.then((value) => {
  					window.location = "http://localhost:8000/";
				});;
			}			
		})
	});

	$('.search-employee-form').on('submit', function(e) {
		e.preventDefault();

		$.ajax({
			url : '/api/employees/search',
			method : 'post',
			data : $(this).serialize(),
			dataType : 'json',
			success : function(response) {
				response = response.data;
				if ($('.list-group-employee').children().length > 0) {
					$('.list-group-employee').slideUp();
				}

				let content = '', countChildren = 0;

				$.each(response, function(i, data) {
					content += `
						<li class="list-group-item">
                            <a href="http://localhost:8000/employees/${data.id}">${data.full_name}</a>
                        </li>
					`;

					countChildren++;
				});

				if (countChildren == 0) {
					content = `
						<li class="list-group-item">
                            Not found.
                        </li>
					`;
				}

				$('.card-header-search-employee').html(`Results (${countChildren})`)

				$('.list-group-employee').html(content);

				$('.list-group-employee').slideDown();					

				$('.card-search-employee-result').slideDown();
			},
			error : function(response) {
				console.log('error');
			}
		});
	});

});