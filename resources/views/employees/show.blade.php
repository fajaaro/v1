@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Profile Page</div>

                    <div class="card-body">
                        <div class="card">
                            <div class="card-body card-body-profile-page">
                                <img class="avatar-show-page mb-4" src="{{ Storage::url($employee->avatar->path) }}">
                                <h5 class="card-title font-weight-bold text-center">{{ $employee->full_name }} <span class="nick-name font-weight-normal">({{ $employee->nick_name }})</span></h5>
                                <h6 class="card-subtitle mb-4 text-muted text-center">Employee</h6>
                                <p class="card-text text-center">Currently {{ $employee->age }} years old, and has been an employee since {{ $employee->created_at->diffForHumans() }}.</p>
                                <p class="card-text text-center address">Address : {{ $employee->address }}</p>
                                <p class="card-text text-center phone-number">Phone Number : {{ $employee->phone_number }}</p>
                                
                                @auth
                                    <div class="row justify-content-center">
                                        <div class="col-md-2">
                                            <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalUpdateEmployee">Update</button>
                                        </div>
                                        <div class="col-md-2">
                                            <form class="delete-employee-form" id="{{ $employee->id }}" method="post" action="{{ route('api.employees.destroy', ['employee' => $employee->id]) }}">
                                                @csrf
                                                @method('DELETE')
                    
                                                <input type="hidden" class="api_token" name="api_token" value="{{ Auth::user()->api_token }}">

                                                <button class="btn btn-danger btn-block" type="submit">Fire</button>
                                            </form>                                        
                                        </div>
                                    </div>
                                @endauth

                            </div>
                        </div>
    
                    </div>


                </div>
            </div>
        </div>
    </div>

    @auth
        <!-- Modal Update Employee -->
        
        <div class="modal fade bd-example-modal-lg" id="modalUpdateEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-dark text-white">
                        <h5 class="modal-title" id="exampleModalLongTitle">Update Employee</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="update-employee-form" id="{{ $employee->id }}" method="post" action="{{ route('api.employees.update', ['employee' => $employee->id]) }}" enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <div class="form-group">
                                <label class="label-input-avatar" for="inputAvatar">Avatar</label>
                                <img class="avatar-modal mb-2" src="{{ Storage::url($employee->avatar->path) }}">
                                <input type="file" class="form-control-file input-change-avatar" name="avatar" id="inputAvatar">
                            </div>                        

                            <input type="hidden" class="api_token" name="api_token" value="{{ Auth::user()->api_token }}">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputName">Full Name</label>
                                    <input type="text" class="form-control" name="full_name" id="inputName" placeholder="Full Name" value="{{ $employee->full_name }}" readonly>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputNickName">Nick Name</label>
                                    <input type="text" class="form-control" name="nick_name" id="inputNickName" placeholder="Nick Name" value="{{ $employee->nick_name }}">
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputBirthdate">Date</label>
                                    <input type="text" class="form-control" name="birthdate" id="inputBirthdate" value="{{ $employee->birthdate }}" readonly="">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="inputAddress">Address</label>
                                <input type="text" class="form-control" name="address" id="inputAddress" placeholder="Address" value="{{ $employee->address }}">
                            </div>
                            
                            <div class="form-group">
                                <label for="inputPhoneNumber">Phone Number</label>
                                <input type="text" class="form-control" name="phone_number" id="inputPhoneNumber" placeholder="Phone Number" value="{{ $employee->phone_number }}">
                            </div>
                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-dark">Update Employee</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- End Modal Update Employee -->
    @endauth
@endsection
