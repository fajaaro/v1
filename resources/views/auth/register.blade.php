@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header navbar-dark bg-dark text-white">{{ __('Register') }}</div>

                    <div class="card-body">
                        <form method="post" action="{{ route('api.register') }}">
                            @csrf
                            
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputName">Name</label>
                                    <input type="text" class="form-control" name="name" id="inputName" placeholder="Name" value="{{ old('name') }}">                                    
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail">Email</label>
                                    <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Email" value="{{ old('email') }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputPassword">Password</label>
                                    <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Password">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-dark">Sign In</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
