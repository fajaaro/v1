<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://kit.fontawesome.com/9dec657e70.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700|Montserrat:300,400,500,700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/logo.png') }}">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark p-3 mb-5">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">PT Bumi Sentosa</a>
            @auth
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#modalAddEmployee">Add Employee</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#modalSearchEmployee">Search Employee</a>
                        </li>
                    </ul>
                </div>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <div class="dropdown show">
                            <li class="dropdown-toggle text-white user-dropdown" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </li>
                            <div class="dropdown-menu" aria-labelledby="dropdownUser">
                                <form class="logout-form d-none" method="post" action="{{ route('logout') }}">
                                    @csrf
                                </form>
                                <a class="dropdown-item logout-link" href="#">Logout</a>
                            </div>
                        </div>
                    </ul>
                </div>
            @else
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">Register</a>
                        </li>
                    </ul>
                </div>            
            @endauth            
        </div>
    </nav>

    @yield('content')
    
    @auth
        
        <!-- Modal Add Employee -->

        <div class="modal fade bd-example-modal-lg" id="modalAddEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-dark text-white">
                        <h5 class="modal-title" id="exampleModalLongTitle">Add New Employee</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="create-employee-form" method="post" action="{{ route('api.employees.store') }}" enctype="multipart/form-data">
                            @csrf

                            <input type="hidden" class="api_token" name="api_token" value="{{ Auth::user()->api_token }}">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputName">Full Name</label>
                                    <input type="text" class="form-control" name="full_name" id="inputName" placeholder="Full Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputNickName">Nick Name</label>
                                    <input type="text" class="form-control" name="nick_name" id="inputNickName" placeholder="Nick Name">
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputDate">Date</label>
                                    <input type="text" class="form-control" name="date" id="inputDate" placeholder="1-31">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputMonth">Month</label>
                                    <input type="text" class="form-control" name="month" id="inputMonth" placeholder="1-12">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="inputYear">Year</label>
                                    <select id="inputYear" class="form-control" name="year">
                                        <option selected>Choose...</option>
                                        @for ($i = 1950; $i <= 2020; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="inputAddress">Address</label>
                                <input type="text" class="form-control" name="address" id="inputAddress" placeholder="Address">
                            </div>
                            
                            <div class="form-group">
                                <label for="inputPhoneNumber">Phone Number</label>
                                <input type="text" class="form-control" name="phone_number" id="inputPhoneNumber" placeholder="Phone Number">
                            </div>
                            
                            <div class="form-group">
                                <label for="inputAvatar">Avatar</label>
                                <input type="file" class="form-control-file" name="avatar" id="inputAvatar">
                            </div>                        
                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-dark">Add Employee</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- End Modal Add Employee -->

        <!-- Modal Search Employee -->  

        <div class="modal fade bd-example-modal-lg" id="modalSearchEmployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-dark">
                        <h5 class="modal-title text-white" id="exampleModalLabel">Search Employee</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="text-white" aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="search-employee-form" method="post" action="{{ route('api.employees.search') }}">
                            @csrf

                            <input class="form-control mr-sm-2" name="employee_name" type="search" placeholder="Enter employee name" aria-label="Search" required>

                            <div class="card mt-3 card-search-employee-result">
                                <div class="card-header card-header-search-employee">
                                    Results
                                </div>
                                <ul class="list-group list-group-flush list-group-employee">

                                </ul>
                            </div>

                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                            <button class="btn btn-dark my-2 my-sm-0 search-employee-btn" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>    

        <!-- End Modal Search Employee -->

    @endauth

    <div class="footer justify-content-center mt-5">
        <p class="text-center">Made with &hearts; by Fajar Hamdani</p>
    </div>
    
    <script src="https://code.jquery.com/jquery-3.5.0.js" integrity="sha256-r/AaFHrszJtwpe+tHyNi/XCfMxYpbsRg2Uqn0x3s2zc=" crossorigin="anonymous"></script>    
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>
