@extends('layouts.app')

@section('content')
    <div class="container">
        
        <div class="row justify-content-center">
            <div class="col-md-11">
                @if (Session::has('status'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('status') }}
                    </div>
                @endif                
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="card">
                    <div class="card-header">Employee List</div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Avatar</th>                                        
                                    <th scope="col">Full Name</th>
                                    <th scope="col">Nick Name</th>
                                    <th scope="col">Age</th>
                                    <th scope="col">Birthdate</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Phone Number</th>
                                </tr>
                            </thead>
                            <tbody class="employee-table-body">
                                @forelse ($employees as $employee)
                                    <tr>
                                        <th scope="row" class="employee-num">{{ $loop->iteration }}</th>
                                        <td><img class="avatar" src="{{ Storage::url($employee->avatar->path) }}"></td>
                                        <td><a href="{{ route('employees.show', ['employee' => $employee->id]) }}">{{ $employee->full_name }}</a></td>
                                        <td>{{ $employee->nick_name }}</td>
                                        <td>{{ $employee->age }}</td>
                                        <td>{{ $employee->birthdate }}</td>
                                        <td>{{ $employee->address }}</td>
                                        <td>{{ $employee->phone_number }}</td>
                                    </tr>
                                @empty
                                    <p class="no-employees">There are currently no registered employees.</p>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
