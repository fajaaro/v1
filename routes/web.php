<?php

use Illuminate\Support\Facades\Route;

Route::redirect('/home', '/');

Route::get('/', 'EmployeeController@index')->name('home');

Auth::routes((['reset' => false, 'confirm' => false]));

Route::resource('employees', 'EmployeeController')->only(['index', 'show']);