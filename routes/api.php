<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::name('api.')->namespace('Api')->group(function() {
	Route::apiResource('employees', 'EmployeeController');
	Route::post('/employees/search', 'EmployeeController@search')->name('employees.search');

	Route::post('/login', 'LoginController@login')->name('login');
	Route::post('/register', 'RegisterController@register')->name('register');
});

Route::fallback(function() {
	return response()->json([
		'message' => 'Not found.',
	], 404);
});